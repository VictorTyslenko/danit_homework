const API_USERS = "https://ajax.test-danit.com/api/json/users";
const API_POSTS = "https://ajax.test-danit.com/api/json/posts";

const root = document.querySelector(".root");
fetch(API_USERS)
  .then((response) => response.json())
  .then((data) => console.log(data));
class Card {
  constructor(users, posts) {
    (this.users = users), (this.posts = posts);
  }

  render() {
    this.users.forEach((user) => {
      this.posts.forEach((post) => {
        if (post.userId === user.id) {
          root.insertAdjacentHTML(
            "beforeend",
            `
            <div class = "main-wrapp">
            <div class="wrapper">
          <h2 class="name">${user.name} <span>${user.username}</span></h2> 
          <p>${user.email}</p>
          <h3 class="title">${post.title}</h3>
          <h4 class ="body-text">${post.body}</h4>
          <button class="remove-btn" data-number="${post.userId}">delete</button>
</div>
</div>
            `
          );
        }
      });
    });
  }
}

window.onload = function data() {
  fetch(API_USERS)
    .then((response) => response.json())
    .then((data) => {
      // console.log(data);
      const userList = data.map(({ name, email, id, username }) => {
        return {
          name: name,
          username: username,
          email: email,
          id: id,
        };
      });
      fetch(API_POSTS)
        .then((response) => response.json())
        .then((data) => {
          const userPosts = data.map(({ title, body, userId }) => {
            return {
              title: title,
              body: body,
              userId: userId,
            };
          });
          // console.log(userPosts);
          const card = new Card(userList, userPosts);
          card.render();
        });
    });
};

document.addEventListener("click", (event) => {
  const button = event.target;
  if (button.classList.contains("remove-btn")) {
    const cardItem = button.closest(".wrapper");
    cardItem.remove();
    const postId = button.dataset.number;
    console.log(postId);
    fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
      method: "DELETE",
    }).then((data) => {
      console.log(data);
    });
  }
});
