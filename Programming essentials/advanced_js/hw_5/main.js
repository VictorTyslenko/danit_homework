const usersURl = "https://ajax.test-danit.com/api/json/users";
const postsURl = "https://ajax.test-danit.com/api/json/posts";

const div = document.querySelector("#root");

window.onload = function data() {
  fetch(usersURl)
    .then((response) => response.json())
    .then((data) => {
      const result = [...data];
      console.log(result);
      const userList = result.map((user) => {
        return {
          name: user.name,
          email: user.email,
          id: user.id,
        };
      });

      fetch(postsURl)
        .then((response) => response.json())
        .then((data) => {
          const result = [...data];
          console.log(result);
          const postList = result.map((post) => {
            return {
              title: post.title,
              body: post.body,
              userId: post.userId,
            };
          });

          new Card(userList, postList).render();
        });
    });
};

class Card {
  constructor(users, posts) {
    this.users = users;
    this.posts = posts;
  }

  render() {
    this.users.forEach((user) => {
      this.posts.forEach((post) => {
        if (post.userId === user.id) {
          div.insertAdjacentHTML(
            "afterbegin",
            `
    <div class ="cards-content"> 
       <div class="card-item">
            <h1 class="name">${user.name}</h1>
            <h2 class="email"><a href="">${user.email}</a></h2>
            <p class="title">${post.title}</p>
            <p class="description">${post.body}</p>
            <button class="remove-btn" data-number=${post.userId}>del</button>
            </div>
            </div>  
    `
          );
        }
      });
    });
  }
}

document.addEventListener("click", (event) => {
  // const btn = document.querySelector('.remove-btn')
  const button = event.target;
  if (button.classList.contains("remove-btn")) {
    const cardItem = button.closest(".card-item");
    cardItem.remove();
    const postId = button.dataset.number;
    fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
      method: "DELETE",
    }).then((data) => {
      console.log(data);
    });
  }
});
