const btn = document.querySelector('.btn');
const API = 'https://api.ipify.org/?format=json';
const div = document.createElement('div')

async function findIp(){

const getIp = await fetch(API)
const dataIp = await getIp.json()
console.log(dataIp);
const getInfo = await fetch(`http://ip-api.com/json/${dataIp.ip}?lang=ru&fields=continent,country,region,city,district`);
const info = await getInfo.json()


const {continent,country,city,region} = info;
console.log(info);
div.insertAdjacentHTML('afterbegin',`
<div>
    <p>Continent:${continent}</p>
    <p>Country:${country}</p>
    <p>City:${city}</p>
    <p>District:${region}</p>
</div>

`)
document.body.append(div);
};

btn.addEventListener('click',()=>{
  div.innerText = '';  
findIp()
});