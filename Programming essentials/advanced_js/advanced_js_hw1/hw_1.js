// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get userName() {
    return this._name;
  }
  set userName(value) {
    return (this._name = value);
  }

  get userAge() {
    return this._age;
  }
  set userAge(value) {
    return (this._age = value);
  }

  get userSalary() {
    return this._salary;
  }
  set userSalary(value) {
    return (this._salary = value);
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }
}

const john = new Programmer("John Warwick", 30, 40000, "Java");
console.log(john);
const anna = new Programmer("Anna Horowitz", 25, 55000, "Python");
console.log(anna);

const allan = new Programmer("Allan Worth", 45, 100000, "C++");
console.log(allan);
