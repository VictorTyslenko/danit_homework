const books = [
    {
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70,
    },
    {
      author: "Сюзанна Кларк",
      name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
      name: "Дизайн. Книга для недизайнерів.",
      price: 70,
    },
    {
      author: "Алан Мур",
      name: "Неономікон",
      price: 70,
    },
    {
      author: "Террі Пратчетт",
      name: "Рухомі картинки",
      price: 40,
    },
    {
      author: "Анґус Гайленд",
      name: "Коти в мистецтві",
    },
  ];
  

const root = document.querySelector("#root");
const ul = document.createElement("ul");

books.forEach((book, index) => {
  try {
    if (!book.hasOwnProperty("author")) {
      throw new Error(`no autor in ${index + 1} book`);
    }
    if (!book.hasOwnProperty("name")) {
      throw new Error(`no name in ${index + 1} book`);
    }
    if (!book.hasOwnProperty("price")) {
      throw new Error(`no price in ${index + 1} book`);
    }

     for(let key in book){
      

       ul.insertAdjacentHTML('beforeend',`<li>${key}: ${book[key]}</li>`)
      }
      root.append(ul)


    } catch (error) {
      console.log(error);
    }
  });
