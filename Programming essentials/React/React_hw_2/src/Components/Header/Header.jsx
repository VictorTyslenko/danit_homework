import FavouriteLogo from '../Icons/FavouriteLogo'
import CartLogo from '../Icons/CartLogo'
import './Header.scss'
const Header = (props) => {
    const { countCart, countFavourite, itemsInFavourite ,itemsInCart} = props



    return (
        <>

            <div className="header">
                <a className='devices' href=''>Devices list</a>
                <div className='header-svg-wrapp'>
                    <sup className='counter'>{countFavourite}</sup>
                    <FavouriteLogo itemsInFavourite={itemsInFavourite} />
                    <sup className='counter'>{countCart}</sup>
                    <CartLogo itemsInCart={itemsInCart} />

                </div>

            </div>



        </>
    )

}

export default Header