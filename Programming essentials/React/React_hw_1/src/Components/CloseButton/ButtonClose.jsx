import React, {Component} from 'react';
import './ButtonClose.scss'
class ButtonClose extends Component {

    render(){
        const {action} = this.props
return (
    // здесь div вместо кнопки чтобы не было по умолчанию серого background,и плюс этот блок не выполняет функцию кнопки
    <>
      <div className='close-btn' onClick = {action}>
        
      </div>
     </>
)
    }

}
export default ButtonClose;