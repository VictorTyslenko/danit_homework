import { useState } from "react";
import Modal from "../../Modal/Modal";
import Card from "../Card/Card";
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from "react-redux";
import { modalClose, addToCart } from "../../../reducers";



const Cards = () => {
  const dispatch = useDispatch()


  const fetchData = useSelector(state => state.data.data)
  const modal = useSelector(state => state.modal.isModal)

  const [selectedProduct, setSelectedProduct] = useState([])




  return (
    <>
      <div className="products-wrapper">
        {!fetchData ? "Loading" : fetchData.map(product =>
          <Card
            key={product.id}
            setProduct={() => setSelectedProduct(product)}

            item={product}

          />)}

      </div>

      {modal && <Modal text='Do you want to continue?'
        onCancel={() => { dispatch(modalClose()) }}
        onConfirm={() => {

          dispatch(addToCart(selectedProduct))
          dispatch(modalClose())

        }
        }

      />}
    </>
  )

};

Cards.propTypes = {
  onAddItem: PropTypes.func,
  closeModal: PropTypes.func

};

export default Cards;









