import { Link } from 'react-router-dom';
import CartLogo from '../Icons/CartLogo'
import FavouriteLogo from '../Icons/FavouriteLogo'
import './Header.scss'
const Header = (props) => {
    const { countCart, countFavourite, itemsInFavourite, itemsInCart } = props



    return (
        <>

            <div className="header">
                <Link to="/" style={{ textDecoration: 'none' }}> <h1 className='devices'>Devices list</h1></Link>

                <div className='header-svg-wrapp'>
                    <sup className='counter'>{countFavourite}</sup>
                    <Link to="/favourites">
                        <FavouriteLogo itemsInFavourite={itemsInFavourite} />
                    </Link>
                    <sup className='counter'>{countCart}</sup>
                    <Link to="/cartitems">
                        <CartLogo itemsInCart={itemsInCart}/>
                    </Link>


                </div>

            </div>


        </>

    )

}

export default Header