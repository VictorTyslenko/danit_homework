
import { useState } from "react";
import Modal from "../Modal/Modal";

import './CartItems.scss'

const CartItems = (props) => {

    const { itemsInCart, onRemove } = props;
    const [open, setOpen] = useState(false);
    const onModalOpen = () => {
        setOpen(true)

    }
    const closeModal = () => {

        setOpen(false)

    };


    return (

        <>
            {!itemsInCart.length && <div className='cart-empty'>
                <strong> Cart is empty</strong>
            </div>}
            <div className="products-wrapper">
                {itemsInCart.map((el) => <div key={el.id}>

                    <div className="card-wrapper">
                        <div className="remove-card" onClick={() => onModalOpen()}>X</div>
                        <div className="img-wrapper">
                            <img className="image" src={el.img} alt={el.name} />
                        </div>
                        <div className="flex-wrapp">
                            <h1 className="device-name">{el.name}</h1>

                        </div>
                        <p className="art">Art: {el.art}</p>
                        <p className="color">Color: {el.color}</p>
                        <div className="price-wrapp">

                            <p className="price">$ {el.price}</p>


                        </div>
                    </div>
                    {open && <Modal text='Do you want to continue?'
                        onCancel={closeModal}
                        onConfirm={() => {
                            onRemove(el)
                            closeModal()

                        }
                        }

                    />}
                </div>)
                }
            </div>
        </>


    )

};

export default CartItems
