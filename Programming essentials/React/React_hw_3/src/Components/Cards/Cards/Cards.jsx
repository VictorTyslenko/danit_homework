import { useEffect, useState } from "react";
import sendRequest from "../../../Helpers/sendRequest";
import Modal from "../../Modal/Modal";
import Card from "../Card/Card";
import PropTypes from 'prop-types';


const Cards = ({ onAddItem, onAddFavourite, onRemoveFavouriteItem }) => {

  const [open, setOpen] = useState(false);

  const [products, setProducts] = useState([])
  const [selectedProduct, setSelectedProduct] = useState({})
  useEffect(() => {

    sendRequest('./API/products.json')
      .then((data) => {

        setProducts(data)

      })

  }, [])

  const productsList = products.map((product) => <Card key={product.id} item={product} onModalOpen={() => {
    setOpen(true);
    setSelectedProduct(product)

  }} onAddFavourite={() => onAddFavourite(product)}
    onRemoveFavouriteItem={() => { onRemoveFavouriteItem(product) }} />)


  const closeModal = () => {

    setOpen(false)

    setSelectedProduct(null)

  };
  return (
    <>
      <div className="products-wrapper">
        {productsList}
      </div>
      {open && <Modal text='Do you want to continue?'
        onCancel={closeModal}
        onConfirm={() => {
          onAddItem(selectedProduct)
          closeModal()

        }
        }

      />}
    </>
  )

};

Cards.propTypes = {
  onAddItem: PropTypes.func,
  closeModal: PropTypes.func

};

export default Cards;









