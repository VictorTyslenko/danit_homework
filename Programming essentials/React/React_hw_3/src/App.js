import { useState } from "react";
import Header from "./Components/Header/Header";
import Cards from "./Components/Cards/Cards/Cards";
import CartItems from "./Components/CartItems/CartItems";
import { Routes, Route } from 'react-router-dom';
import FavouriteItems from "./Components/FavouriteItems/FavouriteItems";
import './App.scss'


const App = () => {


    const getCard = () => {
        let cart = JSON.parse(localStorage.getItem('cart'));
        return cart
    }
    const getFavourite = () => {
        let favourite = JSON.parse(localStorage.getItem('favourite'));
        return favourite
    }

    const [itemsInCart, setItemsInCart] = useState(localStorage.getItem('cart') ? getCard() : [])

    const [itemsInFavourite, setItemsInFavourite] = useState(localStorage.getItem('favourite') ? getFavourite() : [])



    const onAddItem = (product) => {
        const exist = itemsInCart.find((el) => el.id === product.id);
        if (exist) {
            const newCartItems = itemsInCart.map((x) =>
                x.id === product.id ? { ...exist, quantity: exist.quantity + 1 } : x

            )
            setItemsInCart(newCartItems)
        }


        else {
            const newCartItems = [...itemsInCart, { ...product, quantity: 1 }];
            setItemsInCart(newCartItems)

        }

    }
    localStorage.setItem('cart', JSON.stringify(itemsInCart))


    const onAddFavourite = (product) => {

        const exist = itemsInFavourite.find((el) => el.id === product.id);
        if (exist) {
            const newFavouriteItems = itemsInFavourite.map((x) =>
                x.id === product.id ? { ...exist, quantity: exist.quantity + 1 } : x

            )
            setItemsInFavourite(newFavouriteItems)
        }


        else {
            const newFavouriteItems = [...itemsInFavourite, { ...product, quantity: 1 }];
            setItemsInFavourite(newFavouriteItems)

        }
    }
    localStorage.setItem('favourite', JSON.stringify(itemsInFavourite))

    const onRemoveCartItem = (product) => {
        const exist = itemsInCart.find((el) => el.id === product.id);
        if (exist.quantity === 1) {
            const newCartItems = itemsInCart.filter((el) =>
                el.id !== product.id);
            setItemsInCart(newCartItems);
        } else {
            const newCartItems = itemsInCart.map((el) => el.id === product.id ? { ...exist, quantity: exist.quantity - 1 } : el);
            setItemsInCart(newCartItems)
        }
    };

    const onRemoveFavouriteItem = (product) => {

        const exist = itemsInFavourite.find((el) => el.id === product.id);
        if (exist.quantity === 1) {
            const newFavouriteItems = itemsInFavourite.filter((el) =>
                el.id !== product.id);
            setItemsInFavourite(newFavouriteItems);
        } else {
            const newFavouriteItems = itemsInFavourite.map((el) => el.id === product.id ? { ...exist, quantity: exist.quantity - 1 } : el);
            setItemsInFavourite(newFavouriteItems)
        }

    };


    return (
        <>
            <div className="container">

                <Header countCart={itemsInCart.length}
                    countFavourite={itemsInFavourite.length}
                    itemsInFavourite={itemsInFavourite}
                    itemsInCart={itemsInCart}
                />


                <Routes>
                    <Route path="/cartitems" element={<CartItems itemsInCart={itemsInCart}
                        onRemove={onRemoveCartItem} />} />
                    <Route index element={<Cards onAddItem={onAddItem} onAddFavourite={onAddFavourite}
                        onRemoveFavouriteItem={onRemoveFavouriteItem} />} />
                    <Route path="/favourites" element={<FavouriteItems itemsInFavourite={itemsInFavourite} onRemove={onRemoveFavouriteItem} onAddItem={onAddItem} />} />

                    <Route path='*' element={<p>page is not found</p>} />
                </Routes>


            </div>

        </>

    )

};
export default App


   // if (
        //     itemsInFavourite.find((favItem) => product.id === favItem.id)
        // ) {
        //     setItemsInFavourite(itemsInFavourite.filter(({ id }) => id !== product.id))
        //     return
        // }
        // setItemsInFavourite([...itemsInFavourite, product])


 // const onAddItem = (cart) => {
    //     setItemsInCart((previous) => {

    //         const existingProduct = previous.find((product) => product.id === cart.id)

    //         const cartArray = existingProduct ? [...previous.filter(product => !(product.id === cart.id)), { id: cart.id, quantity: existingProduct.quantity + 1 }]
    //             : [...previous,{ id: cart.id, quantity: 1 }]
    //         localStorage.setItem('cart', JSON.stringify(cartArray))
    //         setItemsInCart(cartArray)
    //         console.log(cartArray);
    //         return cartArray
    //     })
    // }
    // const onAddItem = (product) => {
    //     if (
    //         itemsInCart.find((cart) => product.id === cart.id)
    //     ) {
    //         setItemsInCart(itemsInCart.filter(({ id }) => id !== product.id))
    //         return
    //     }

    //     setItemsInCart([...itemsInCart, product])
    //     localStorage.setItem('cart', JSON.stringify(itemsInCart))
    // }














