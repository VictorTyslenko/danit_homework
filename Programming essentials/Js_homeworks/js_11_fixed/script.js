const passwordForm = document.querySelector( '.password-form' );

const changeIcon = (event) => {
	const iconPassword = event.target; 

	if ( iconPassword.classList.contains( 'fa-eye' ) ) {
		iconPassword.classList.toggle( 'fa-eye-slash' )
	}
}
const changeInput = (event) => {
	const input = event.target.parentNode.querySelector('input');
	if ( input.type === "password" ) {
		input.setAttribute( 'type', 'text' );
	} else {
		input.setAttribute( 'type', 'password' );
	}
}
const inputValidation = (event) => {
	const form = event.target.parentNode;
	const button = form.querySelector( '.btn' );
	const input_1 = form.querySelector( '#one' );
	const input_2 = form.querySelector( '#two' );
	const createP = document.createElement( 'p' );
	const messageError = form.querySelector( 'p.colored' );
	let message = '';
	createP.classList.add('colored');

	if ( input_1.value.length === 0 && input_2.value.length === 0 ) {
		message = 'Введiть пароль'
	} else if ( input_1.value !== input_2.value ) {
		message = 'Значення не спiвпадають'
	} else {
		if ( messageError ) {
			messageError.remove()
		}
		alert( 'You are welcome!' )
	}

	if ( messageError ) {
		messageError.innerText = message
	}else {
		createP.innerText = message
		button.before(createP)
	}

}

passwordForm.addEventListener( 'click', ( event ) => {
	event.preventDefault();

	if ( event.target.nodeName === 'I' ) {
		console.log( event.target );
		changeIcon(event)
		changeInput(event)
	}

	if ( event.target.nodeName === 'BUTTON' ) {
		inputValidation(event);
	}

} )
