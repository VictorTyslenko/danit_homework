const userArray = ["hello", "world", 23, "23", null];
function filterBy(array, type) {
  //   return array.filter((el) => {
  //     if (typeof type === "object") {
  //       return el
  //     } else {
  //       return typeof el !== type;
  //     }
  //   });
  return array.filter((el) => {
    return typeof type === "object" ? el !== null : typeof el !== type;
  });
}

console.log(filterBy(userArray, null));
